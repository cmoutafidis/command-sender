# Command Sender Plugin

This is a plugin that sends commands when a user triggers the PlayerInteractEvent, based on the rules defined in the config.yml

## Usage

When you first install this plugin, the defaul configuration created will look something like this:
```
enabled: true

diamond_pickaxe:
  sneak: true
  right-click: true
  left-click: false
  player: 'ce'

iron_pickaxe:
  sneak: false
  right-click: true
  left-click: false
  console: 'msg %player% Command ran through console!'
```
  
You can define when the command should be fired and if it should be fired from the player or the console.
Use the placeholder %player% to add the players name in the command.