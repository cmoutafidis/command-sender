package gq.goldencraft.commandsender;

import gq.goldencraft.commandsender.commands.ReloadCommand;
import gq.goldencraft.commandsender.listeners.ClickListener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class CommandSenderPlugin extends JavaPlugin {

    private static Plugin plugin;

    public static Plugin getPlugin() {
        return CommandSenderPlugin.plugin;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        CommandSenderPlugin.plugin = this;
        this.registerCommands();
        this.registerEvents();
        this.saveDefaultConfig();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    private void registerEvents() {
        this.getServer().getPluginManager().registerEvents(new ClickListener(), this);
    }

    private void registerCommands() {
        this.getCommand("reloadCommandSender").setExecutor(new ReloadCommand());
    }
}
