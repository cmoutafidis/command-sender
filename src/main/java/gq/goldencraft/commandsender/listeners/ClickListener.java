package gq.goldencraft.commandsender.listeners;

import gq.goldencraft.commandsender.CommandSenderPlugin;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

public class ClickListener implements Listener {

    private final Plugin plugin = CommandSenderPlugin.getPlugin();

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        final FileConfiguration config = this.plugin.getConfig();
        if (config.getBoolean("enabled")) {
            if (event.getItem() != null) {
                final String[] eventParts = event.getAction().toString().toLowerCase().split("_");
                final String eventItem = event.getItem().getType().toString().toLowerCase();

                final Boolean actionEvent = (Boolean) config.get(eventItem + "." + eventParts[0] + "-" + eventParts[1]);
                final Boolean sneakFlag = ((Boolean) config.get(eventItem + ".sneak"));

                if (actionEvent != null && actionEvent && sneakFlag != null && sneakFlag == event.getPlayer().isSneaking()) {
                    final String playerMessage = ((String) config.get(eventItem + ".player"));
                    final String consoleMessage = ((String) config.get(eventItem + ".console"));

                    if (playerMessage != null) {
                        event.getPlayer().chat(PlaceholderAPI.setPlaceholders(event.getPlayer(), playerMessage));
                    }
                    if (consoleMessage != null) {
                        this.plugin.getServer().dispatchCommand(this.plugin.getServer().getConsoleSender(), PlaceholderAPI.setPlaceholders(event.getPlayer(), consoleMessage));
                    }
                }
            }
        }
    }
}
