package gq.goldencraft.commandsender.commands;

import gq.goldencraft.commandsender.CommandSenderPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

public class ReloadCommand implements CommandExecutor {

    private final Plugin plugin = CommandSenderPlugin.getPlugin();

    public boolean onCommand(final CommandSender commandSender, final Command command, final String s, final String[] strings) {
        this.plugin.reloadConfig();
        commandSender.sendMessage("Config Reloaded");
        return true;
    }

}
